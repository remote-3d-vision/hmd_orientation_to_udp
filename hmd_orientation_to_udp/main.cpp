
#include <stdio.h>

extern "C"
{
#include "udp/udp.h"
}

#include <Windows.h>

//allow use of trig and M_PI
#define _USE_MATH_DEFINES
#include <math.h>

#include "OVR.h"
#include "Kernel/OVR_String.h"

#pragma comment (lib, "libovr.lib")
#pragma comment (lib, "winmm.lib")

using namespace OVR;

int main(int argc, char *argv[])
{
    if (argc < 3)
	{
        printf("Usage: hmd_orientation_to_udp ip_address port\n");
        return -1;
	}
    char *ip_address = argv[1];
    int port = strtol(argv[2], NULL, 10);

    //Find the HMD device and sensor
    OVR::System::Init();

    Ptr<DeviceManager> device_manager = 0;
    Ptr<HMDDevice> hmd_device = 0;
    Ptr<SensorDevice> sensor = 0;
    SensorFusion sensor_fusion;
    
    device_manager = *DeviceManager::Create();
    hmd_device = device_manager->EnumerateDevices<HMDDevice>().CreateDevice();
    if (!hmd_device)
	{
        printf("Failed to create hmd device\n");
        getchar();
        return -1;
	}

    sensor = hmd_device->GetSensor();

    HMDInfo hmd_info;
    hmd_device->GetDeviceInfo(&hmd_info);

    if (sensor)
        sensor_fusion.AttachToSensor(sensor);

    //setup the UDP context
    socket_ctx_t sender_ctx;
    sender_init(&sender_ctx, ip_address, port);

    Quatf q;
    float pitch, yaw, roll;
    int pitch_to_send, yaw_to_send, roll_to_send;
    char string_to_send[15];
    while (1)
	{
        //get predicted orientation 30ms from now
        q = sensor_fusion.GetPredictedOrientation(0.03f);
        q.GetEulerAngles<Axis_Y, Axis_X, Axis_Z>(&yaw, &pitch, &roll);

        //convert radians to degrees pitch range [0, 179000], yaw range [0, 359000]
        pitch_to_send = (int)(pitch*180*1000/M_PI) + 90000;
        yaw_to_send = (int)(yaw*180*1000/M_PI) + 180000;
        roll_to_send = (int)(roll*180*1000/M_PI); //not used
        printf("pitch: %06d, yaw: %06d, roll: %06d\n", pitch_to_send, yaw_to_send, roll_to_send);

        //format the data to send
		_snprintf(string_to_send, 15, "[%06d%06d]\0", yaw_to_send, pitch_to_send);

        //send the orientation over udp
        sender_send(&sender_ctx, string_to_send, 15);

        //send data at a rate of 240Hz
        Sleep(4);
	}

    sensor.Clear();
    hmd_device.Clear();
    device_manager.Clear();

    OVR::System::Destroy();

    return 0;
}
